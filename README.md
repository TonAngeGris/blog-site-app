# Blog Site Application

A site written with Node.js/Express.js/PostgreSQL/Handlebars/Boostrap stack.

### Requirements
- [Node.js](https://nodejs.org/) v16+

### Installation
1. Clone the project
```sh
git clone https://gitlab.com/TonAngeGris/blog-site-app.git
cd blog-site-app
```
2. Install dependencies
```sh
npm i
```

### Usage
1. Run the app:
```sh
npm run start
```
2. Enjoy it!
