-- postgres
CREATE TABLE "user" (
	user_id SERIAL PRIMARY KEY,
	user_name VARCHAR(32) NOT NULL,
	user_password VARCHAR(255) NOT NULL
	user_role VARCHAR(32) DEFAULT 'USER'
); 

CREATE TABLE article (
	article_id SERIAL PRIMARY KEY,
	article_title VARCHAR(64) NOT NULL,
	article_description VARCHAR(255) NOT NULL,
	article_content TEXT NOT NULL,
	author_id INT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	
	FOREIGN KEY (author_id) REFERENCES "user"(user_id)
);

CREATE TABLE "token" (
	refresh_token TEXT PRIMARY KEY,
	user_id INT NOT NULL,
	
	FOREIGN KEY (user_id) REFERENCES "user"(user_id)
);