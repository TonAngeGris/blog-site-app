const db = require('../dbConfig');

class UserDAO {
  static async findByName(username) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `SELECT user_id, user_name, user_password, user_role
                         FROM "user" 
                         WHERE user_name = $1`;
      const candidate = await db.query(queryText, [username]);
      await db.query('COMMIT');

      return candidate.rows[0];
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async addUser(username, password) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `INSERT INTO "user" (user_name, user_password)
                         VALUES ($1, $2) 
                         RETURNING *`;
      const queryValues = [username, password];
      const newUser = await db.query(queryText, queryValues);
      await db.query('COMMIT');

      return newUser.rows[0];
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async findUsersByPattern(search) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `SELECT "user".user_id, "user".user_name, "user".user_role, COUNT(article.author_id) AS "article_count"
                         FROM "user"
                         LEFT OUTER JOIN "article" ON "user".user_id = "article".author_id
                         WHERE LOWER("user".user_name) LIKE LOWER($1)
                         GROUP BY "user".user_id
                         ORDER BY "user".user_id ASC`;
      const users = await db.query(queryText, [`%${search}%`]);

      await db.query('COMMIT');

      return users.rows;
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }
}

module.exports = UserDAO;
