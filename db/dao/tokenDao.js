const db = require('../dbConfig');

class TokenDAO {
  static async findRefreshOne(userId) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `SELECT refresh_token, user_id
                         FROM "token"
                         WHERE user_id = $1`;
      const tokenData = await db.query(queryText, [userId]);
      await db.query('COMMIT');

      return tokenData.rows[0];
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async addRefreshOne(userId, token) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `INSERT INTO "token" (refresh_token, user_id)
                         VALUES ($1, $2)`;
      const queryValues = [token, userId];
      await db.query(queryText, queryValues);
      await db.query('COMMIT');
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async updateRefreshOne(userId, token) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `UPDATE "token"
                         SET refresh_token = $1
                         WHERE user_id = $2`;
      const queryValues = [token, userId];
      await db.query(queryText, queryValues);
      await db.query('COMMIT');
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async deleteRefreshOne(userId) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `DELETE FROM "token"
                         WHERE user_id = $1`;
      await db.query(queryText, [userId]);
      await db.query('COMMIT');
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }
}

module.exports = TokenDAO;
