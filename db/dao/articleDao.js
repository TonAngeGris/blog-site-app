const db = require('../dbConfig');

class ArticleDAO {
  static async createArticle(title, description, content, authorId) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `INSERT INTO article (article_title, article_description, article_content, author_id)
                         VALUES ($1, $2, $3, $4)`;
      const queryValues = [title, description, content, authorId];
      await db.query(queryText, queryValues);
      await db.query('COMMIT');
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async findArticleById(articleId) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `SELECT "user".user_name, article.article_id, article.article_title, 
                               article.article_description, article.article_content, article.author_id, article.created_at
                         FROM article 
                         INNER JOIN "user" ON "user".user_id = article.author_id 
                         WHERE article_id = $1`;
      const article = await db.query(queryText, [articleId]);
      await db.query('COMMIT');

      return article.rows[0];
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async findArticlesByTitle(search) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `SELECT "user".user_name, article.article_id, article.article_title,
                                article.article_description, article.article_content, article.created_at
                         FROM article
                         INNER JOIN "user" ON "user".user_id = article.author_id
                         WHERE LOWER(article.article_title) LIKE LOWER($1)
                         ORDER BY article.created_at DESC`;
      const articles = await db.query(queryText, [`%${search}%`]);

      await db.query('COMMIT');

      return articles.rows;
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async findArticlesByUser(search) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `SELECT "user".user_name, article.article_id, article.article_title, 
                               article.article_description, article.article_content, article.created_at
                         FROM article 
                         INNER JOIN "user" ON "user".user_id = article.author_id
                         WHERE LOWER("user".user_name) LIKE LOWER($1)
                         ORDER BY article.created_at DESC`;
      const articles = await db.query(queryText, [`%${search}%`]);

      await db.query('COMMIT');

      return articles.rows;
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async updateArticle(articleId, title, description, content) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = `UPDATE article 
                         SET article_title = $1, article_description = $2, article_content = $3
                         WHERE article_id = $4`;
      const queryValues = [title, description, content, articleId];
      await db.query(queryText, queryValues);
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }

  static async deleteArticle(articleId) {
    const client = await db.connect();

    try {
      await db.query('BEGIN');
      const queryText = 'DELETE FROM article WHERE article_id = $1';
      await db.query(queryText, [articleId]);

      await db.query('COMMIT');
    } catch (err) {
      await db.query('ROLLBACK');
    } finally {
      client.release();
    }

    return null;
  }
}

module.exports = ArticleDAO;
