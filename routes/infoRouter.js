const router = require('express').Router();
const InfoController = require('../controllers/infoController');

router.get('/about', InfoController.about);
// router.get('/help', InfoController.help);

module.exports = router;
