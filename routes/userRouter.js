const router = require('express').Router();
const { body } = require('express-validator');
const UserController = require('../controllers/userController');

router.post(
  '/registration',
  body('username')
    .isLength({ min: 5, max: 32 })
    .matches(/^[A-Za-z0-9_-]{5,32}$/)
    .withMessage('You can only use Latin letters, underscores and dashes!'),
  body('password').isLength({ min: 8, max: 128 }),
  UserController.postRegistration
);
router.get('/registration', UserController.getRegistration);

router.post('/login', UserController.postLogin);
router.get('/login', UserController.getLogin);

router.post('/logout', UserController.logout);

router.post('/refresh', UserController.refresh);

router.get('/users', UserController.getUsers);

module.exports = router;
