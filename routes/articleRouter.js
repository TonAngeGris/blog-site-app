const router = require('express').Router();
const { body } = require('express-validator');
const ArticleController = require('../controllers/articleController');
const authMiddleware = require('../middlewares/authMiddleware');

router.post(
  '/articles/create',
  authMiddleware,
  body('title').isLength({ min: 10, max: 64 }),
  body('description').isLength({ min: 10, max: 255 }),
  ArticleController.postCreateArticle
);
router.get('/articles/create', authMiddleware, ArticleController.getCreateArticle);

router.get(['/', '/articles'], ArticleController.getArticles);

router.get('/articles/:id', ArticleController.getOneArticle);
router.get('/articles/edit/:id', authMiddleware, ArticleController.getEditArticle);
router.post(
  '/articles/edit/:id',
  authMiddleware,
  body('title').isLength({ min: 10, max: 64 }),
  body('description').isLength({ min: 10, max: 255 }),
  ArticleController.postEditArticle
);
router.post('/articles/delete/:id', authMiddleware, ArticleController.deleteArticle);

module.exports = router;
