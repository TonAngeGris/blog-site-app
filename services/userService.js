const bcrypt = require('bcrypt');
const UserDAO = require('../db/dao/userDao');
const ApiError = require('../helpers/apiError');
const TokenService = require('./tokenService');

class UserService {
  static async registration(username, password) {
    // Check if such an username already exists
    const candidate = await UserDAO.findByName(username);
    if (candidate) {
      throw ApiError.BadRequest(`This username (${username}) is already taken!`);
    }

    // Add a new user to the database
    const hashedPassword = await bcrypt.hashSync(password, 5);
    const newUser = await UserDAO.addUser(username, hashedPassword);

    // Generate tokens
    const payload = {
      id: newUser.user_id,
      username: newUser.user_name,
      role: newUser.user_role,
    };
    const refreshToken = TokenService.generateRefreshToken(payload);
    const accessToken = TokenService.generateAccessToken(payload);
    await TokenService.saveRefreshToken(refreshToken, newUser.user_id);

    return { accessToken, user: payload };
  }

  static async login(username, password) {
    const user = await UserDAO.findByName(username);
    if (!user) {
      throw ApiError.BadRequest(`Incorrect username or password!`);
    }

    const isPasswordsEqual = await bcrypt.compare(password, user.user_password);
    if (!isPasswordsEqual) {
      throw ApiError.BadRequest(`Incorrect username or password!`);
    }

    const payload = {
      id: user.user_id,
      username: user.user_name,
      role: user.user_role,
    };
    const refreshToken = TokenService.generateRefreshToken(payload);
    const accessToken = TokenService.generateAccessToken(payload);
    await TokenService.saveRefreshToken(refreshToken, user.user_id);

    return { accessToken, user: payload };
  }

  static async refresh(accessToken) {
    const userData = TokenService.decodeToken(accessToken);
    if (!userData) {
      throw ApiError.AuthorizationError();
    }

    const refreshToken = await TokenService.findRefreshToken(userData.id);
    const isRefreshTokenValid = TokenService.validateRefreshToken(refreshToken);
    // TODO: Handle refresh token expiration error
    if (!refreshToken || !isRefreshTokenValid) {
      throw ApiError.AuthorizationError();
    }

    const payload = {
      id: userData.id,
      username: userData.username,
      role: userData.role,
    };
    const newAccessToken = TokenService.generateAccessToken(payload);

    /* 
    accessToken: string;
    user: {
        id: any;
        username: any;
        role: any;
    };
    */
    return { accessToken: newAccessToken, user: payload };
  }

  static async logout(accessToken) {
    const userData = TokenService.decodeToken(accessToken);
    if (!userData) {
      throw ApiError.AuthorizationError();
    }

    await TokenService.removeRefreshToken(userData.id);
  }

  static async getUsers(search, accessToken) {
    const users = await UserDAO.findUsersByPattern(search);

    const decoded = TokenService.decodeToken(accessToken);

    return { users, user: decoded };
  }
}

module.exports = UserService;
