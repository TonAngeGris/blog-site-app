const jwt = require('jsonwebtoken');
const TokenDAO = require('../db/dao/tokenDao');

class TokenService {
  static generateRefreshToken(payload) {
    return jwt.sign(payload, `${process.env.JWT_REFRESH_SECRET}`, {
      algorithm: 'HS256',
      expiresIn: '30d',
    });
  }

  static generateAccessToken(payload) {
    return jwt.sign(payload, `${process.env.JWT_ACCESS_SECRET}`, {
      algorithm: 'HS256',
      expiresIn: '30m',
    });
  }

  static validateRefreshToken(token) {
    try {
      const userData = jwt.verify(token, `${process.env.JWT_REFRESH_SECRET}`);
      return userData;
    } catch (err) {
      return null;
    }
  }

  static validateAccessToken(token) {
    try {
      const userData = jwt.verify(token, `${process.env.JWT_ACCESS_SECRET}`);
      return userData;
    } catch (err) {
      return null;
    }
  }

  static async saveRefreshToken(token, userId) {
    const tokenData = await TokenDAO.findRefreshOne(userId);

    if (tokenData) {
      await TokenDAO.updateRefreshOne(userId, token);
      return;
    }

    await TokenDAO.addRefreshOne(userId, token);
  }

  static async findRefreshToken(userId) {
    try {
      const tokenData = await TokenDAO.findRefreshOne(userId);
      return tokenData.refresh_token;
    } catch (err) {
      return null;
    }
  }

  static async removeRefreshToken(userId) {
    await TokenDAO.deleteRefreshOne(userId);
  }

  static decodeToken(token) {
    try {
      const decodedData = jwt.decode(token);
      return decodedData;
    } catch (err) {
      return null;
    }
  }
}

module.exports = TokenService;
