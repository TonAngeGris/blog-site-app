const TokenService = require('./tokenService');
const ArticleDAO = require('../db/dao/articleDao');
const ApiError = require('../helpers/apiError');

class ArticleService {
  static async postCreateArticle(accessToken, title, description, content) {
    const { id: authorId } = TokenService.decodeToken(accessToken);

    await ArticleDAO.createArticle(title, description, content, authorId);
  }

  static getCreateArticle(accessToken) {
    const decoded = TokenService.decodeToken(accessToken);
    if (decoded) {
      return { id: decoded.id, username: decoded.username };
    }

    return null;
  }

  static async getOneArticle(articleId, accessToken) {
    const article = await ArticleDAO.findArticleById(articleId);
    if (!article) {
      throw ApiError.NotFoundError();
    }

    let isAuthor = false;
    const decoded = TokenService.decodeToken(accessToken);
    if (decoded?.id === article.author_id) {
      isAuthor = true;
    }

    return { article, user: decoded, isAuthor };
  }

  static async getArticles(search, accessToken) {
    const titleSearch = search?.search_title ?? '';
    const userSearch = search?.search_user ?? '';

    let articles;
    if (!userSearch) {
      articles = await ArticleDAO.findArticlesByTitle(titleSearch);
    } else {
      articles = await ArticleDAO.findArticlesByUser(userSearch);
    }

    const decoded = TokenService.decodeToken(accessToken);

    return { articles, user: decoded };
  }

  static async postEditArticle(articleId, title, description, content) {
    await ArticleDAO.updateArticle(articleId, title, description, content);
  }

  static async getEditArticle(articleId, accessToken) {
    const article = await ArticleDAO.findArticleById(articleId);
    if (!article) {
      throw ApiError.NotFoundError();
    }

    const decoded = TokenService.decodeToken(accessToken);

    return { article, user: decoded };
  }

  static async deleteArticle(articleId) {
    await ArticleDAO.deleteArticle(articleId);
  }
}

module.exports = ArticleService;
