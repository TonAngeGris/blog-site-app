const TokenService = require('./tokenService');

class InfoService {
  static about(accessToken) {
    const decoded = TokenService.decodeToken(accessToken);
    if (decoded) {
      return { user: decoded };
    }

    return null;
  }
}

module.exports = InfoService;
