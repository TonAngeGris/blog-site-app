class ApiError extends Error {
  status;
  errors;

  constructor(status, message, errors = []) {
    super(message);
    this.status = status;
    this.errors = errors;
  }

  static AuthorizationError() {
    return new ApiError(401, 'User is not authorized!');
  }

  static NotFoundError() {
    return new ApiError(404, 'Requested resource does not exist!');
  }

  static BadRequest(message, errors = []) {
    return new ApiError(400, message, errors);
  }
}

module.exports = ApiError;
