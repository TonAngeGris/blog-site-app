const { validationResult } = require('express-validator');
const ApiError = require('../helpers/apiError');
const UserService = require('../services/userService');

class UserController {
  static async postRegistration(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return next(ApiError.BadRequest('Validation error!', errors.array()));
      }

      const { username, password } = req.body;
      const userData = await UserService.registration(username, password);

      res.cookie('accessToken', userData.accessToken, {
        maxAge: 15 * 60 * 1000,
        httpOnly: true,
        path: '/',
        sameSite: true,
      });

      return res.redirect('/');
    } catch (err) {
      return next(err);
    }
  }

  static async getRegistration(req, res) {
    return res.status(200).render('users/registration.hbs');
  }

  static async postLogin(req, res, next) {
    try {
      const { username, password } = req.body;
      const userData = await UserService.login(username, password);

      res.cookie('accessToken', userData.accessToken, {
        maxAge: 15 * 60 * 1000,
        httpOnly: true,
        path: '/',
        sameSite: true,
      });

      return res.redirect('/');
    } catch (err) {
      return next(err);
    }
  }

  static async getLogin(req, res) {
    res.status(200).render('users/login.hbs');
  }

  static async logout(req, res, next) {
    try {
      const { accessToken } = req.cookies;
      await UserService.logout(accessToken);
      res.clearCookie('accessToken');

      return res.redirect('/');
    } catch (err) {
      return next(err);
    }
  }

  static async refresh(req, res, next) {
    try {
      const { accessToken } = req.cookies;
      const userData = await UserService.refresh(accessToken);

      return res.status(200).send({ accessToken: userData.accessToken });
    } catch (err) {
      return next(err);
    }
  }

  static async getUsers(req, res, next) {
    try {
      const search = req.query?.search ?? '';
      const { accessToken } = req.cookies;

      const data = await UserService.getUsers(search, accessToken);

      return res.status(200).render('users/users.hbs', {
        users: data.users,
        user: data.user,
      });
    } catch (err) {
      return next(err);
    }
  }
}

module.exports = UserController;
