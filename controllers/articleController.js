const { validationResult } = require('express-validator');
const ApiError = require('../helpers/apiError');
const ArticleService = require('../services/articleService');

class ArticleController {
  static async postCreateArticle(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return next(ApiError.BadRequest('Validation error!', errors.array()));
      }

      const { accessToken } = req.cookies;
      const { title, description, content } = req.body;

      await ArticleService.postCreateArticle(
        accessToken,
        title,
        description,
        content
      );
      return res.redirect('/articles');
    } catch (err) {
      return next(err);
    }
  }

  static async getCreateArticle(req, res, next) {
    try {
      const { accessToken } = req.cookies;
      const userData = ArticleService.getCreateArticle(accessToken);

      return res.status(200).render('articles/create.hbs', { user: userData });
    } catch (err) {
      return next(err);
    }
  }

  static async getOneArticle(req, res, next) {
    try {
      const { id: articleId } = req.params;
      const { accessToken } = req.cookies;

      const data = await ArticleService.getOneArticle(articleId, accessToken);

      return res.status(200).render('articles/show.hbs', {
        article: data.article,
        user: data.user,
        isAuthor: data.isAuthor,
      });
    } catch (err) {
      return next(err);
    }
  }

  static async getArticles(req, res, next) {
    try {
      // const search = req.query?.search ?? '';
      const search = req.query;
      const { accessToken } = req.cookies;

      const data = await ArticleService.getArticles(search, accessToken);

      return res.status(200).render('articles/articles.hbs', {
        articles: data.articles,
        user: data.user,
      });
    } catch (err) {
      return next(err);
    }
  }

  static async postEditArticle(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return next(ApiError.BadRequest('Validation error!', errors.array()));
      }

      const { id: articleId } = req.params;
      const { title, description, content } = req.body;

      await ArticleService.postEditArticle(articleId, title, description, content);

      return res.status(200).redirect(`/articles/${articleId}`);
    } catch (err) {
      return next(err);
    }
  }

  static async getEditArticle(req, res, next) {
    try {
      const { id: articleId } = req.params;
      const { accessToken } = req.cookies;

      const data = await ArticleService.getEditArticle(articleId, accessToken);

      return res.status(200).render('articles/edit.hbs', {
        article: data.article,
        user: data.user,
      });
    } catch (err) {
      return next(err);
    }
  }

  static async deleteArticle(req, res, next) {
    try {
      const { id: articleId } = req.params;

      await ArticleService.deleteArticle(articleId);

      // TODO: float window with a question about deletion
      return res.redirect('/articles');
    } catch (err) {
      return next(err);
    }
  }
}

module.exports = ArticleController;
