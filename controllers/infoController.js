const InfoService = require('../services/infoService');

class InfoController {
  static async about(req, res, next) {
    try {
      const { accessToken } = req.cookies;

      const data = await InfoService.about(accessToken);

      return res.status(200).render('info/about.hbs', { user: data?.user });
    } catch (err) {
      return next(err);
    }
  }
}

module.exports = InfoController;
