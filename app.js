require('dotenv').config();
const cookieParser = require('cookie-parser');
const express = require('express');
const cors = require('cors');
const exphbs = require('express-handlebars');
const morgan = require('morgan');
const path = require('path');

const exphbsConfig = require('./config/handlebars');
const articleRouter = require('./routes/articleRouter');
const userRouter = require('./routes/userRouter');
const infoRouter = require('./routes/infoRouter');
const errorMiddleware = require('./middlewares/errorMiddleware');

const app = express();

// Handlebars configuration
app.engine('hbs', exphbs(exphbsConfig));
app.set('view engine', 'hbs');

app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, '/public')));
app.use(
  '/css',
  express.static(path.join(__dirname, '/node_modules/bootstrap/dist/css'))
);
app.use(
  '/css',
  express.static(path.join(__dirname, '/node_modules/bootstrap-icons/font'))
);
app.use(
  '/js',
  express.static(path.join(__dirname, '/node_modules/bootstrap/dist/js'))
);

app.use(
  cors({
    credentials: true,
    origin: `http://localhost:${process.env.SERVER_PORT}/`,
  })
);
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser());

app.use('/', articleRouter);
app.use('/', userRouter);
app.use('/', infoRouter);
app.use(errorMiddleware);

app.listen(process.env.SERVER_PORT, (err) =>
  err
    ? console.log(err)
    : console.log(`Server is running on ${process.env.SERVER_PORT}...`)
);
