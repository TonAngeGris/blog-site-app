const config = {
  layoutsDir: './views/layouts',
  defaultLayout: 'main',
  extname: '.hbs',
  helpers: {
    shortDateFormat(date) {
      const newDate = date.toLocaleString('ru-RU', {
        year: '2-digit',
        month: '2-digit',
        day: '2-digit',
      });

      const newDayTime = date.toLocaleString('ru-RU', {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
      });

      // format example: 15.11.22 15:25:05
      return `${newDate} ${newDayTime}`;
    },

    eq(v1, v2) {
      return v1 === v2;
    },

    or(v1, v2) {
      return v1 || v2;
    },

    gt(v1, v2) {
      return v1 > v2;
    },
  },
};

module.exports = config;
