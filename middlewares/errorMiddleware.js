const ApiError = require('../helpers/apiError');

// eslint-disable-next-line no-unused-vars
module.exports = (err, req, res, next) => {
  if (err instanceof ApiError) {
    switch (err.status) {
      case 400:
        return res
          .status(err.status)
          .render('errors/400.hbs', { message: err.message, errors: err.errors });
      case 401:
        return res
          .status(err.status)
          .render('errors/401.hbs', { message: err.message, errors: err.errors });
      case 404:
        return res
          .status(err.status)
          .render('errors/404.hbs', { message: err.message, errors: err.errors });
      default:
        break;
    }
  }

  return res
    .status(500)
    .render('errors/500.hbs', { message: 'Internal server error!' });
};
