const fetch = require('node-fetch');
const ApiError = require('../helpers/apiError');
const TokenService = require('../services/tokenService');

module.exports = async (req, res, next) => {
  try {
    const accessToken = req.cookies.accessToken;
    if (!accessToken) {
      return next(ApiError.AuthorizationError());
    }

    const userData = TokenService.validateAccessToken(accessToken);
    if (!userData) {
      // Silent server-side token refreshing
      const response = await fetch(
        `http://localhost:${process.env.SERVER_PORT}/refresh`,
        {
          method: 'POST',
          headers: {
            cookie: req.headers.cookie,
          },
        }
      );
      const data = await response.json();

      res.cookie('accessToken', data.accessToken, {
        maxAge: 15 * 60 * 1000,
        httpOnly: true,
        path: '/',
        sameSite: true,
      });
    }

    return next();
  } catch (err) {
    return next(ApiError.AuthorizationError());
  }
};
